from distutils.core import setup

install_requires = ['pyliblo', 
					'scipy',
					'numpy',
					'h5py',
					'protobuf',
					'hdf5storage']

setup(name='muse-player',
      version='1.0',
      description='MusePlayer is a utility for recording, replaying, rerouting, and converting EEG and accelerometer data from Interaxon Muse EEG devices.',
      author='Interaxon',
      package_dir={'museplayer': 'src'},
      packages=['museplayer'],
      install_requires=install_requires
      )